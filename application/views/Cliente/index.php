<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Clientes</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
</head>

<body>
    <div class="container">
        <h1 class="font-weight-bold text-center">Creación de clientes</h1>
        <form action="" id="frmCliente">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group required">
                        <label for="" class="control-label">Codigo cliente</label>
                        <input type="text" name="codigo" id="" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group required">
                        <label for="" class="control-label">Nombres</label>
                        <input type="text" name="nombres" id="" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <button type="submit" id="btnGuardar" class="btn btn-info">Guardar</button>
            </div>
        </form>
    </div>

    </div>

    <script type="text/javascript">
		var baseurl = "<?php echo base_url();?>";
	</script>
    <script>
        
        const form = document.getElementById('frmCliente');

        form.addEventListener('submit', async (e) => {
            e.preventDefault();
            const data = new FormData(form)
            const serializedData = serialize(data);
            const url = `${baseurl}/cliente/Create_customer`;

            const options ={
                method:'POST',
                body:JSON.stringify(serializedData),
                headers:{
                    "Content-Type": "application/json",
                }
            }
            console.log(options)
            try {
                const response = await fetch(url,options);
                const json = await response.json();
                console.log(json)
            } catch (error) {
                console.log('error',error)
            }

        })


        function serialize(data) {
            let obj = {};
            for (let [key, value] of data) {
                if (obj[key] !== undefined) {
                    if (!Array.isArray(obj[key])) {
                        obj[key] = [obj[key]];
                    }
                    obj[key].push(value);
                } else {
                    obj[key] = value;
                }
            }
            return obj;
        }
    </script>
</body>

</html>