<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *
 * Controller Cliente
 *
 * This controller for ...
 *
 * @package   CodeIgniter
 * @category  Controller CI
 * @author    Setiawan Jodi <jodisetiawan@fisip-untirta.ac.id>
 * @author    Raul Guerrero <r.g.c@me.com>
 * @link      https://github.com/setdjod/myci-extension/
 * @param     ...
 * @return    ...
 *
 */

class Cliente extends CI_Controller
{
    
  public function __construct()
  {
    parent::__construct();
    $this->load->model('cliente_model');
  }

  public function index()
  {
    $this->load->view('cliente/index');
    
  }

  public function Create_customer(){
     $datos=json_decode($this->input->raw_input_stream);

     $data = array(
      'nomcli' => $datos->nombres, 
      'codcli' => $datos->codigo, 
    );

    $cliente_model = new Cliente_model();
    $response = $cliente_model->Create_customer($data);
    echo json_encode($response);

    
  }


}


/* End of file Cliente.php */
/* Location: ./application/controllers/Cliente.php */