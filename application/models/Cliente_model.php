<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * Model Cliente_model
 *
 * This Model for ...
 * 
 * @package		CodeIgniter
 * @category	Model
 * @author    Setiawan Jodi <jodisetiawan@fisip-untirta.ac.id>
 * @link      https://github.com/setdjod/myci-extension/
 * @param     ...
 * @return    ...
 *
 */

class Cliente_model extends CI_Model {

  // ------------------------------------------------------------------------

  private $table_name = 'cliente';

  public function __construct()
  {
    parent::__construct();
  }

  // ------------------------------------------------------------------------


  // ------------------------------------------------------------------------
  public function index()
  {
    // 
  }

  public function Create_customer($data){

    $this->db->insert($this->table_name,$data);
    $rows_affeceted = $this->db->affected_rows();

    $response['success']=$rows_affeceted>0?true:false;
    $response['message']=$response['success']?'CLIENTE INSERTADO CON EXITO':'ERROR AL INSERTAR CLIENTE';
    return $response;

  }

  // ------------------------------------------------------------------------

}

/* End of file Cliente_model.php */
/* Location: ./application/models/Cliente_model.php */